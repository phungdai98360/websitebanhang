package ptithcm.entity;
import javax.persistence.*;

@Entity
@Table(name = "CTHoaDon")
public class CTHoaDon {
	@Id
	@GeneratedValue
	@Column(name = "Id")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "MaHoaDon")
	private HoaDon hoadon;
	
	@ManyToOne
	@JoinColumn(name = "MaSP")
	private SanPham sanpham;

	@Column(name = "SoLuong")
	private int soluong;
	@Column(name = "TongGia")
	private int tonggia;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public HoaDon getHoadon() {
		return hoadon;
	}
	public void setHoadon(HoaDon hoadon) {
		this.hoadon = hoadon;
	}
	public SanPham getSanpham() {
		return sanpham;
	}
	public void setSanpham(SanPham sanpham) {
		this.sanpham = sanpham;
	}
	public int getSoluong() {
		return soluong;
	}
	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}
	public int getTonggia() {
		return tonggia;
	}
	public void setTonggia(int tonggia) {
		this.tonggia = tonggia;
	}
	
	

}
