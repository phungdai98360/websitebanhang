package ptithcm.entity;
import java.util.Collection;

import javax.persistence.*;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "KhachHang")
public class KhachHang {
	@Id
	@Column(name = "Username")
	private String username;
	@Column(name = "Password")
	private String password;
	@Column(name = "Fullname")
	private String fullname;
	@Column(name = "Email")
	@Email()
	public String email;
	@Column(name = "DiaChi")
	private String diachi;
	@OneToMany(mappedBy = "khachang",fetch = FetchType.EAGER)
	private Collection<HoaDon> hoadon;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDiachi() {
		return diachi;
	}
	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}
	public Collection<HoaDon> getHoadon() {
		return hoadon;
	}
	public void setHoadon(Collection<HoaDon> hoadon) {
		this.hoadon = hoadon;
	}
	

}
