package ptithcm.entity;
import javax.persistence.*;
@Entity
@Table(name = "Admin")
public class Admin {
	@Id
	@Column(name = "Username")
	private String username;
	@Column(name = "Password")
	private String password;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
