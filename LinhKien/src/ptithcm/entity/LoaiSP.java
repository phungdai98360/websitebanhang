package ptithcm.entity;
import javax.persistence.*;
import java.util.Collection;
@Entity
@Table(name = "LoaiSP")
public class LoaiSP {
	@Id
	@Column(name = "MaLoaiSP")
	private String maloaisp;
	@Column(name = "TenLoaiSp")
	private String tenloaisp;
	@OneToMany(mappedBy = "loaisp",fetch = FetchType.EAGER)
	private Collection<SanPham> sanpham;
	public String getMaloaisp() {
		return maloaisp;
	}
	public void setMaloaisp(String maloaisp) {
		this.maloaisp = maloaisp;
	}
	public String getTenloaisp() {
		return tenloaisp;
	}
	public void setTenloaisp(String tenloaisp) {
		this.tenloaisp = tenloaisp;
	}
	public Collection<SanPham> getSanpham() {
		return sanpham;
	}
	public void setSanpham(Collection<SanPham> sanpham) {
		this.sanpham = sanpham;
	}
	

}
