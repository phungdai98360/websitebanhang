package ptithcm.entity;
import java.util.Collection;

import javax.persistence.*;

@Entity
@Table(name = "SanPham")
public class SanPham {
	@Id
	@Column(name = "MaSP")
	private String masp;
	@Column(name = "TenSP")
	private String tensp;
	@Column(name = "ChiTiet")
	private String chitiet;
	@Column(name = "Gia")
	private Integer gia;
	@Column(name = "SoLuong")
	private Integer soluong;
	@Column(name = "HinhAnh")
	private String hinhanh;
	
	@OneToMany(mappedBy = "sanpham",fetch = FetchType.EAGER)
	private Collection<CTHoaDon> cthoadon;

	@ManyToOne
	@JoinColumn(name = "MaLoaiSP")
	private LoaiSP loaisp;
	public String getMasp() {
		return masp;
	}

	public void setMasp(String masp) {
		this.masp = masp;
	}

	public String getTensp() {
		return tensp;
	}

	public void setTensp(String tensp) {
		this.tensp = tensp;
	}

	public String getChitiet() {
		return chitiet;
	}

	public void setChitiet(String chitiet) {
		this.chitiet = chitiet;
	}

	public Integer getGia() {
		return gia;
	}

	public void setGia(Integer gia) {
		this.gia = gia;
	}

	public Integer getSoluong() {
		return soluong;
	}

	public void setSoluong(Integer soluong) {
		this.soluong = soluong;
	}

	public String getHinhanh() {
		return hinhanh;
	}

	public void setHinhanh(String hinhanh) {
		this.hinhanh = hinhanh;
	}

	public Collection<CTHoaDon> getCthoadon() {
		return cthoadon;
	}

	public void setCthoadon(Collection<CTHoaDon> cthoadon) {
		this.cthoadon = cthoadon;
	}

	public LoaiSP getLoaisp() {
		return loaisp;
	}

	public void setLoaisp(LoaiSP loaisp) {
		this.loaisp = loaisp;
	}
	

}
