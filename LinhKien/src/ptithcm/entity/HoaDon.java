package ptithcm.entity;
import java.util.Collection;
import java.util.Date;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "HoaDon")
public class HoaDon {
	@Id
	@Column(name = "MaHoaDon")
	@GeneratedValue
	private int mahoadon;
	@Column(name = "ChiTiet")
	private String chitiet;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date ngaymua;
	@ManyToOne
	@JoinColumn(name = "Username")
	private KhachHang khachang;
	
	@OneToMany(mappedBy = "hoadon",fetch = FetchType.EAGER)
	private Collection<CTHoaDon> cthoadon;

	public int getMahoadon() {
		return mahoadon;
	}

	public void setMahoadon(int mahoadon) {
		this.mahoadon = mahoadon;
	}

	public String getChitiet() {
		return chitiet;
	}

	public void setChitiet(String chitiet) {
		this.chitiet = chitiet;
	}

	public Date getNgaymua() {
		return ngaymua;
	}

	public void setNgaymua(Date ngaymua) {
		this.ngaymua = ngaymua;
	}

	public KhachHang getKhachang() {
		return khachang;
	}

	public void setKhachang(KhachHang khachang) {
		this.khachang = khachang;
	}

	public Collection<CTHoaDon> getCthoadon() {
		return cthoadon;
	}

	public void setCthoadon(Collection<CTHoaDon> cthoadon) {
		this.cthoadon = cthoadon;
	}

	
	
	
	
}
