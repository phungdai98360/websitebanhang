package ptithcm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ptithcm.entity.LoaiSP;
import ptithcm.entity.SanPham;
@Transactional
@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	SessionFactory factory;
	List<Object[]> ListSanPham;
	public List<Object[]> listsanpham()
	{
		Session session=factory.getCurrentSession();
		String hql="FROM SanPham";
		Query query=session.createQuery(hql);
		ListSanPham=query.list();
		return ListSanPham;
	}
	public boolean CheckMasp(String masp)
	{
		boolean trangthai=false;
		Session session=factory.getCurrentSession();
		String hql="FROM SanPham WHERE masp="+"'"+masp+"'";
		Query query=session.createQuery(hql);
		List<Object[]> list=query.list();
		if(list!=null&&list.size()>0)
		{
			trangthai=true;
		}
		return trangthai;
	}
	 public String Xulichuoi(String str)
	    {
	        str=str.trim();
	        str=str.replaceAll("\\s+","");
	        
	        return str;
	    }
	public boolean ChechKyTu(String sData)
	{
		boolean trangthai=false;
		for (int i = 0; i < sData.length(); i++) {
            if (Character.isLetter(sData.charAt(i))) {
//                System.out.println("Way2 - Not a Number");
//                break;
            	trangthai=false;
            	break;
            }
            if (i + 1 == sData.length()) {
                //System.out.println("Way2 - Is a Number");
            	trangthai=true;
            }
        }
		return trangthai;
	}
	@RequestMapping("/login")
	public String Login()
	{
		return "Admin/AdminLogin";
	}
	@RequestMapping(value = "/login",method = RequestMethod.POST)
	public String Login(HttpServletRequest request,ModelMap model)
	{
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		Session session=factory.getCurrentSession();
		if(username.isEmpty())
		{
			model.addAttribute("notice1", "username không để trống");
			model.addAttribute("password",password );
			return "Admin/AdminLogin";
		}
		if(password.isEmpty())
		{
			model.addAttribute("notice2", "password không để trống");
			model.addAttribute("username",username );
			return "Admin/AdminLogin";
		}
		else
		{
			String hql="FROM Admin WHERE username="+"'"+username+"'"+" "+"AND password="+"'"+password+"'";
			Query query=session.createQuery(hql);
			List<Object[]> list=query.list();
			if(list!=null&&list.size()>0)
			{
				model.addAttribute("username", username);
				model.addAttribute("list", listsanpham());
				return "Admin/TrangChuAdmin";
			}
			else
			{
				model.addAttribute("username",username );
				model.addAttribute("password",password );
				model.addAttribute("message", "Tên đăng nhập hoặc mật khẩu không đúng");
				return "Admin/AdminLogin";
			}
		}
		
		
	}
	@RequestMapping(value = "/insert",method = RequestMethod.GET)
	public String Insert()
	{
		return "Admin/Insert";
	}
	@RequestMapping(value = "/insert",method = RequestMethod.POST)
	public String Insert(HttpServletRequest request,ModelMap model)
	{
		String masp=request.getParameter("masp");
		String tensp=request.getParameter("tensp");
		String ctsp=request.getParameter("ctsp");
		String giasp=request.getParameter("giasp");
		int the_gia=0;
		if(giasp.isEmpty()==false&&ChechKyTu(giasp)==true)
		{
			 the_gia=Integer.parseInt(Xulichuoi(giasp));
		}
		
		String solsp=request.getParameter("solsp");
		int the_sl=0;
		if(solsp.isEmpty()==false&&ChechKyTu(solsp)==true)
		{
			the_sl=Integer.parseInt(solsp);
		}
		String malsp=request.getParameter("malsp");
		String ha=request.getParameter("ha");
		if(masp.isEmpty()||tensp.isEmpty()||ctsp.isEmpty()||giasp.isEmpty()||solsp.isEmpty()||malsp.isEmpty()||ha.isEmpty())
		{
		 model.addAttribute("message", "bạn đã bỏ trống 1 số mục");	
		 model.addAttribute("masp",masp);
		 model.addAttribute("tensp", tensp);
		 model.addAttribute("ctsp", ctsp);
		 model.addAttribute("giasp", giasp);
		 model.addAttribute("solsp", solsp);
		 model.addAttribute("malsp", malsp);
		 model.addAttribute("ha", ha);
		 return "Admin/Insert";
		}
		if(the_gia<=0||the_sl<=0)
		{
			model.addAttribute("message", "bạn nhập giá trị <0 hoặc nhập sai kiểu số");
			 model.addAttribute("masp",masp);
			 model.addAttribute("tensp", tensp);
			 model.addAttribute("ctsp", ctsp);
			 model.addAttribute("giasp", giasp);
			 model.addAttribute("solsp", solsp);
			 model.addAttribute("malsp", malsp);
			 model.addAttribute("ha", ha);
			return "Admin/Insert";
		}
		if(CheckMasp(masp))
		{
			model.addAttribute("message", "Mã sản phẩm đã bị trùng");
			 model.addAttribute("masp",masp);
			 model.addAttribute("tensp", tensp);
			 model.addAttribute("ctsp", ctsp);
			 model.addAttribute("giasp", giasp);
			 model.addAttribute("solsp", solsp);
			 model.addAttribute("malsp", malsp);
			 model.addAttribute("ha", ha);
			return "Admin/Insert";
		}
		else
		{
			
			Session session=factory.openSession();
			Transaction t=session.beginTransaction();
			LoaiSP l=new LoaiSP();
			l.setMaloaisp(malsp);
			SanPham s=new SanPham();
			s.setMasp(masp);
			s.setTensp(tensp);
			s.setChitiet(ctsp);
			s.setGia(the_gia);
			s.setSoluong(the_sl);
			s.setLoaisp(l);
			s.setHinhanh(ha);
			try {
				session.save(s);
				t.commit();
				model.addAttribute("message","thêm thành công");
				model.addAttribute("list", listsanpham());
				return "Admin/TrangChuAdmin";
			} catch (Exception e) {
				// TODO: handle exception
				t.rollback();
				model.addAttribute("message", "thêm thất bai");
			}
			finally
			{
				session.close();
			}
			
		}
		return "Admin/Insert";
	}
	@RequestMapping(value = "/delete",method = RequestMethod.GET)
	public String Delete(@RequestParam("masp") String masp,ModelMap model)
	{
		model.addAttribute("masp", masp);
		return "Admin/Delete";
	}
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	public String Delete(ModelMap model,HttpServletRequest request)
	{
		Session session=factory.openSession();
		Transaction t=session.beginTransaction();
		String masp=request.getParameter("masp");
		SanPham s=new SanPham();
		s.setMasp(masp);
		
		try {
			session.delete(s);
			t.commit();
			model.addAttribute("message", "Xóa thành công");
			model.addAttribute("list", listsanpham());
			return "Admin/TrangChuAdmin";
		} catch (Exception e) {
			// TODO: handle exception
			t.rollback();
			model.addAttribute("message", "xóa thất bại");
		}
		finally
		{
			session.close();
		}
		return "Admin/Delete";
	}
	@RequestMapping(value = "/update",method = RequestMethod.GET)
	public String Update(@RequestParam("masp") String masp,
			@RequestParam("tensp") String tensp,
			@RequestParam("ctsp") String ctsp,
			@RequestParam("giasp") int giasp,
			@RequestParam("solsp") int solsp,
			@RequestParam("malsp") String malsp,
			@RequestParam("ha") String ha,
			ModelMap model)
	{
		 model.addAttribute("masp",masp);
		 model.addAttribute("tensp", tensp);
		 model.addAttribute("ctsp", ctsp);
		 model.addAttribute("giasp", giasp);
		 model.addAttribute("solsp", solsp);
		 model.addAttribute("malsp", malsp);
		 model.addAttribute("ha", ha);
		return "Admin/Update";
	}
	@RequestMapping(value = "/update",method = RequestMethod.POST)
	public String Update(HttpServletRequest request,ModelMap model)
	{
		String masp=request.getParameter("masp");
		String tensp=request.getParameter("tensp");
		String ctsp=request.getParameter("ctsp");
		String giasp=request.getParameter("giasp");
		int the_gia=0;
		if(giasp.isEmpty()==false&&ChechKyTu(giasp)==true)
		{
			 the_gia=Integer.parseInt(Xulichuoi(giasp));
		}
		
		String solsp=request.getParameter("solsp");
		int the_sl=0;
		if(solsp.isEmpty()==false&&ChechKyTu(solsp)==true)
		{
			the_sl=Integer.parseInt(solsp);
		}
		String malsp=request.getParameter("malsp");
		String ha=request.getParameter("ha");
		if(masp.isEmpty()||tensp.isEmpty()||ctsp.isEmpty()||giasp.isEmpty()||solsp.isEmpty()||malsp.isEmpty()||ha.isEmpty())
		{
		 model.addAttribute("message", "bạn đã bỏ trống 1 số mục");	
		 model.addAttribute("masp",masp);
		 model.addAttribute("tensp", tensp);
		 model.addAttribute("ctsp", ctsp);
		 model.addAttribute("giasp", giasp);
		 model.addAttribute("solsp", solsp);
		 model.addAttribute("malsp", malsp);
		 model.addAttribute("ha", ha);
		 return "Admin/Update";
		}
		if(the_gia<=0||the_sl<=0)
		{
			model.addAttribute("message", "bạn nhập giá trị <0 hoặc nhập sai kiểu số");
			 model.addAttribute("masp",masp);
			 model.addAttribute("tensp", tensp);
			 model.addAttribute("ctsp", ctsp);
			 model.addAttribute("giasp", giasp);
			 model.addAttribute("solsp", solsp);
			 model.addAttribute("malsp", malsp);
			 model.addAttribute("ha", ha);
			return "Admin/Update";
		}
		else
		{
			
			Session session=factory.openSession();
			Transaction t=session.beginTransaction();
			LoaiSP l=new LoaiSP();
			l.setMaloaisp(malsp);
			SanPham s=new SanPham();
			s.setMasp(masp);
			s.setTensp(tensp);
			s.setChitiet(ctsp);
			s.setGia(the_gia);
			s.setSoluong(the_sl);
			s.setLoaisp(l);
			s.setHinhanh(ha);
			try {
				session.update(s);
				t.commit();
				model.addAttribute("message","sửa thành công");
				model.addAttribute("list", listsanpham());
				return "Admin/TrangChuAdmin";
			} catch (Exception e) {
				// TODO: handle exception
				t.rollback();
				model.addAttribute("message", "sửa thất bai");
			}
			finally
			{
				session.close();
			}
			
		}
		return "Admin/Update";
	}
}
