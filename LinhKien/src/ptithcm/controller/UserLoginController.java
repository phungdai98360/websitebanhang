package ptithcm.controller;

import java.util.Date;
import java.util.List;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ptithcm.bean.UserBean;
import ptithcm.entity.HoaDon;
import ptithcm.entity.KhachHang;
import ptithcm.entity.SanPham;
@Transactional
@Controller
@RequestMapping("/user")
public class UserLoginController {
	@Autowired
	SessionFactory factory;
	
	@Autowired
	JavaMailSender mailer;
	
	@RequestMapping(value = "/userlogin",method = RequestMethod.GET)
	public String user()
	{
		return "User/UserLogin";
	}
	@RequestMapping(value = "/login",method = RequestMethod.GET)
	public String Login()
	{
		return "User/DangNhap";
	}
	public boolean CheckMaKH(String makh)
	{
		boolean trangthai=false;
		Session session=factory.getCurrentSession();
		String hql="FROM KhachHang WHERE username="+"'"+makh+"'";
		
		Query query=session.createQuery(hql);
		List<Object[]> l=query.list();
		if(l!=null&&l.size()>0)
		{
			trangthai=true;
		}
		return trangthai;
	}
	@RequestMapping(value = "/login",method = RequestMethod.POST)
	public String Login(HttpServletRequest request,ModelMap model)
	{
		
		String username1=request.getParameter("username");
		String password1=request.getParameter("password");
		if(username1.isEmpty())
		{
			model.addAttribute("notice1", "Vui lòng không để trống tài khoản");
			model.addAttribute("username", username1);
			model.addAttribute("password", password1);
			return "User/DangNhap";
		}
		if(password1.isEmpty())
		{
			model.addAttribute("notice2", "Vui lòng không để trống mật khẩu");
			model.addAttribute("username", username1);
			model.addAttribute("password", password1);
			return "User/DangNhap";
		}
		else
		{
			Session session=factory.getCurrentSession();
			String hql="FROM KhachHang WHERE username="+"'"+username1+"'"+" "+"AND password="+"'"+password1+"'";
			Query query=session.createQuery(hql);
			UserBean u=new UserBean();
			List<UserBean> l=query.list();
			//model.addAttribute("list", l);
			if(l!=null&&l.size()>0)
			{
				u.setUsername(username1);
				model.addAttribute("k",u.getUsername());
				Session session1=factory.getCurrentSession();
				String hql1="FROM SanPham";
				Query query1=session.createQuery(hql1);
				List<SanPham> list=query1.list();
				model.addAttribute("list", list);
				return "TrangChu/TrangChu";
			}
			else {
				model.addAttribute("message", "Tài khoản hoặc mật khẩu không chính xác");
				return "User/DangNhap";
			}
		}
		

	 }
	@RequestMapping(value = "/login1",method = RequestMethod.GET)
	public String InsertUser()
	{
		return "User/DangKi";
	}
	@RequestMapping(value = "/login1",method = RequestMethod.POST)
	public String InsertUser(HttpServletRequest request,ModelMap model)
	{
		String usernamedk=request.getParameter("usernamedk");
		String passworddk=request.getParameter("passworddk");
		String passworddk1=request.getParameter("passworddk1");
		String fullname=request.getParameter("fullname");
		String mail=request.getParameter("mail");
		String diachi=request.getParameter("diachi");
		if(usernamedk.isEmpty()||passworddk.isEmpty()||passworddk1.isEmpty()||fullname.isEmpty()||mail.isEmpty()||diachi.isEmpty())
		{
			model.addAttribute("message", "Bạn đã bỏ trống 1 số mục");
			model.addAttribute("fullname", fullname);
			model.addAttribute("usernamedk", usernamedk);
			model.addAttribute("mail", mail);
			model.addAttribute("diachi", diachi);
			model.addAttribute("passworddk", passworddk);
			model.addAttribute("passworddk1", passworddk1);
			return  "User/DangKi";
		}
		if(CheckMaKH(usernamedk))
		{
			model.addAttribute("message", "Tên tài khoản đã tồn tại");
			model.addAttribute("fullname", fullname);
			model.addAttribute("usernamedk", usernamedk);
			model.addAttribute("mail", mail);
			model.addAttribute("diachi", diachi);
			model.addAttribute("passworddk", passworddk);
			model.addAttribute("passworddk1", passworddk1);
			return  "User/DangKi";
		}
		if(passworddk.equals(passworddk1)==false)
		{
			model.addAttribute("message", "Hai mật khẩu không trùng nhau");
			model.addAttribute("fullname", fullname);
			model.addAttribute("usernamedk", usernamedk);
			model.addAttribute("mail", mail);
			model.addAttribute("diachi", diachi);
			model.addAttribute("passworddk", passworddk);
			model.addAttribute("passworddk1", passworddk1);
			return  "User/DangKi";
		}
		
		
		else
		{
			Session session=factory.openSession();
			Transaction t=session.beginTransaction();
			KhachHang k=new KhachHang();
			k.setUsername(usernamedk);
			k.setPassword(passworddk);
			k.setFullname(fullname);
			k.setEmail(mail);
			k.setDiachi(diachi);
			try {
				session.save(k);
				t.commit();
				model.addAttribute("message", "Đăng kí thành công");
				//return "TrangChu/TrangChu";
				
			} catch (Exception e) {
				// TODO: handle exception
				t.rollback();
				model.addAttribute("fullname", fullname);
				model.addAttribute("usernamedk", usernamedk);
				model.addAttribute("mail", mail);
				model.addAttribute("diachi", diachi);
				model.addAttribute("passworddk", passworddk);
				model.addAttribute("passworddk1", passworddk1);
				model.addAttribute("message", "Đăng kí thất bại kiểm tra định dang email");
			}
		}
		return "User/DangKi";
	}
	
	@RequestMapping(value = "/dathang",method = RequestMethod.GET)
	public String DatHang(ModelMap model,
			@RequestParam("masp") String masp,
			@RequestParam("makh") String makh,
			@RequestParam("tensp") String tensp,
			@RequestParam("gia") int gia,
			@RequestParam("ha") String ha
			)
	{
		model.addAttribute("masp", masp);
		model.addAttribute("makh", makh);
		model.addAttribute("tensp", tensp);
		model.addAttribute("gia", gia);
		model.addAttribute("ha", ha);
		return "User/DatHang";
	}
	
	@RequestMapping("/dathang")
	public String DatHang(ModelMap model,HttpServletRequest request)
	{
		String masp=request.getParameter("masp");
		String makh=request.getParameter("makh");
		String soluong=request.getParameter("soluong");
		int the_soluong=Integer.parseInt(soluong);
		Session session=factory.getCurrentSession();
//		String sql="EXEC SP_DatHang1 'xavi123','SP5',2";
		String sql="EXEC SP_DatHang1"+" "+"'"+makh+"'"+","+"'"+masp+"'"+","+the_soluong;
		Query query=session.createSQLQuery(sql);
		int result=query.executeUpdate();
		if(result>0)
		{
			model.addAttribute("message", "Success");
			String sql1="SELECT * FROM HoaDon WHERE Username="+"'"+makh+"'"+" "+"and Month(NgayMua)=Month(GETDATE()) and Day(NgayMua)=Day(Getdate()) and Year(NgayMua)=Year(getdate())";
			SQLQuery query1=session.createSQLQuery(sql1);
			query1.addEntity(HoaDon.class);
			List<Object[]> listmahd=query1.list();
			model.addAttribute("list", listmahd);
			
			String sql2="SELECT * FROM KhachHang WHERE Username="+"'"+makh+"'";
			SQLQuery query2=session.createSQLQuery(sql2);
			//query2.executeUpdate();
			query2.addEntity(KhachHang.class);
			String email="";
			List<KhachHang> listemail=query2.list();
			email=listemail.get(0).getEmail();
			String from="mixaolanot123@gmail.com";
			String to=email;
			String subject="Xác nhân đặt hàng";
			String body="Cảm ơn bạn đã đặt hàng của chúng tôi";
			//gui mail
			try {
				MimeMessage mail=mailer.createMimeMessage();
				MimeMessageHelper helper=new MimeMessageHelper(mail);
				helper.setFrom(from, from);
				helper.setTo(to);
				helper.setReplyTo(from, from);
				helper.setSubject(subject);
				helper.setText(body,true);
				mailer.send(mail);
				//model.addAttribute("message", "Gửi mail thành công");
			} catch (Exception e) {
				// TODO: handle exception
				//model.addAttribute("message", "Gửi mail thât bại");
			}
			return "User/Notice";
			
		}
		else {
			model.addAttribute("message", "Fail !!!");
		}
		return "User/Notice";
	}
	//kiem tra don hang
	@RequestMapping(value = "/kiemtra",method = RequestMethod.GET)
	public String KiemTra()
	{
		return "User/MaKTDonHang";
	}
	@RequestMapping(value = "/kiemtra",method = RequestMethod.POST)
	public String KiemTra(ModelMap model,HttpServletRequest request)
	{
		String mahd=request.getParameter("mahd");
		int the_mahd=Integer.parseInt(mahd);
		Session session=factory.getCurrentSession();
		String hql="FROM CTHoaDon WHERE hoadon.mahoadon="+the_mahd;
		Query query=session.createQuery(hql);
		List<Object[]> list=query.list();
		model.addAttribute("list", list);
		return "User/Kiemtra";
	}

}
