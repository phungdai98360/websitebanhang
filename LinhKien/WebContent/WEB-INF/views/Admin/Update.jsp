<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
 <%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="f" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Insert title here</title>
	<base href="${pageContext.servletContext.contextPath}/">
	<link rel="stylesheet"  href="Css/StyleUser.css">
	<link rel="stylesheet"  href="Bootstrap/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"  href="Bootstrap/bootstrap/css/font-awesome.min.css">
    
    <link href="Bootstrap/bootstrap/css/mdb.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/7578a82dfb.js"></script>
</head>
<body>
	<header>
		<div class="container-fluid">
       
	       <div class="row">
	           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thanhheader">
               	 <div class="row">
                   
                   <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-push-1 image-logo" >
                       <img width="80" height="75" src="Bootstrap/bootstrap/logo.jpg" class="position-image"/>
                   </div>
                   
                   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-sm-push-1 search">
                       <input type="search" class="form-control-search"
                        name="" id="input" value="" required="required" placeholder="Bạn tìm gì...">
                          <button 
                               type="button"
                               class="button-search">
                               <span class="fa fa-search mr-5"/>
                         </button>
                   </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-sm-push-1" >
	                    <div class="row">

		                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin">
		                        	<i class="fas fa-cart-plus icon-user"></i>
		                        	<p class="admin">Giỏ hàng</p>
		                        	
		                        </a>
                             </div>
                             <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin">
		                        	<i class="fas fa-truck icon-user"></i>
		                        	<p class="admin">Kiểm tra</p>
		                        	
		                        </a>
                             </div>
                             <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin">
		                        	<i class="fas fa-home icon-user"></i>
		                        	<p class="admin">Trang chủ</p>
		                        	
		                        </a>
                             </div>
                             
	                   </div>
                    </div>
                   
                </div>
               </div>
	       </div>
       
       </div>
    </header>
    
    <!-- contexxt -->
    <br/>
    <br/>
    <br/>
    
  
    
    
    
    <div class="container">
       
       <div class="row">
         
         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
           
         </div>
         
         <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="background: whitesmoke;">
            <form action="admin/update.htm" method="POST" role="form">
              <legend>Sửa sản phẩm</legend>
            
              <div class="form-group">
                <input type="text" readonly name="masp" value="${masp}" class="form-control" id="text1" placeholder="Nhập mã sản phẩm"><br>
                <p class="notice1" style="color: red;">Vui lòng không để trống</p><br>
                <input type="text" name="tensp" value="${tensp}" class="form-control" id="text2" placeholder="Nhập tên sản phẩm"><br>
                <p class="notice2" style="color: red;">Vui lòng không để trống</p><br>
                <input type="text" name="ctsp" value="${ctsp}" class="form-control" id="text3" placeholder="Nhập chi tiết sản phẩm"><br>
                <p class="notice3" style="color: red;">Vui lòng không để trống</p><br>
                <input type="text" name="giasp" value="${giasp}" class="form-control" id="text4" placeholder="Nhập giá sản phẩm"><br>
                <p class="notice4" style="color: red;">Vui lòng không để trống</p><br>
                <input type="text" name="solsp" value="${solsp}" class="form-control" id="text5" placeholder="Nhập số lượng sản phẩm"><br>
                <p class="notice5" style="color: red;">Vui lòng không để trống</p><br>
                <input type="text" name="malsp" value="${malsp}" class="form-control" id="text6" placeholder="Nhập mã loại sản phẩm"><br>
                <p class="notice6" style="color: red;">Vui lòng không để trống</p><br>
                <input type="text" name="ha" value="${ha}" class="form-control" id="text7" placeholder="Nhập đường dẫn ảnh"><br>
                <p class="notice7" style="color: red;">Vui lòng không để trống</p><br>
      
              </div>
            
              
            
              <button type="submit" class="btn btn-primary">Sửa</button>
            </form>
            <p>${message}</p>
         </div>
         
         
       </div>
       
     </div>
    
    
    
    
      

    <!-- footer -->
    <br/>
    <br/>
	<footer>
        <div class="footer-tgdd">
          <ul class="ul1">
            <li><a>Tìm hiểu về mua trả góp</a></li>
            <li><a>Chính sách bảo hành</a></li>
            <li><a>Chính sách đổi trả</a></li>
            <li><a>Giao hàng & Thanh toán</a></li>
            <li>
            <div class="dropdown">
              <div  data-toggle="dropdown">Xem thêm
              <span class="caret"></span></div>
              <ul class="dropdown-menu">
                <li><a href="#">Hướng dẫn mua online</a></li>
                <li><a href="#">Mua hàng doanh nghiệp</a></li>
                <li><a href="#">Phiếu mua hàng</a></li>
                <li><a href="#">In hóa đơn điện tử</a></li>
                <li><a href="#">Quy chế hoạt động</a></li>
                <li><a href="#">Nội quy cửa hàng</a></li>
                <li><a href="#">Chất lượng phục vụ</a></li>
                <li><a href="#">Thông tin trao thưởng</a></li>
                <li><a href="#">Chính sách khui hộp sản phẩm Apple</a></li>
              </ul>
            </div>
           </li>
          </ul>
          <ul class="ul2">
            <li><a>Giới thiệu công ty (mwg.vn)</a></li>
            <li><a>Tuyển dụng</a></li>
            <li><a>Gửi góp ý, khiếu nại</a></li>
            <li><a>Tìm siêu thị </a></li>
            <li><a>Xem bản mobile</a></li>
          </ul>
          <ul class="ul2">
            <li><a>Gọi mua hàng &nbsp; 1800.1060</a></li>
            <li><a>Gọi khiếu nại &nbsp; &nbsp;  1800.1062</a></li>
            <li><a>Gọi bảo hành &nbsp; &nbsp; 1800.1064</a></li>
            <li><a>Kĩ thuật &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 1800.1763</a></li>
            <li><a><i class="icontgdd-bct"></i></a></li>
          </ul>
          <ul class="ul2">
            <li>
              <a><i class="fab fa-facebook-square"></i>&nbsp; 3.3tr</a>
              &nbsp;
              &nbsp;
              <a><i class="fab fa-youtube"></i>  624.2k</a>
            </li>
            
            
          </ul>
        </div>
      </footer>
      <script src="Js/xuliUserLogin.js"></script>
	<script src="Bootstrap/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="Bootstrap/bootstrap/js/bootstrap.min.js"></script>
    <script>
    	$(document).ready(function(){
    		$('#text1').on('keyup',function(ev){
    			if($(this).val().length<1)
    				{
    				$('.notice1').show(500);
    				}
    			else
    				{
    				$('.notice1').hide(500);
    				}
    		});
    		$('#text2').on('keyup',function(ev){
    			if($(this).val().length<1)
    				{
    				$('.notice2').show(500);
    				}
    			else
    				{
    				$('.notice2').hide(500);
    				}
    		});
    		$('#text3').on('keyup',function(ev){
    			if($(this).val().length<1)
    				{
    				$('.notice3').show(500);
    				}
    			else
    				{
    				$('.notice3').hide(500);
    				}
    		});
    		$('#text4').on('keyup',function(ev){
    			if($(this).val().length<1)
    				{
    				$('.notice4').show(500);
    				}
    			else
    				{
    				$('.notice4').hide(500);
    				}
    		});
    		$('#text5').on('keyup',function(ev){
    			if($(this).val().length<1)
    				{
    				$('.notice5').show(500);
    				}
    			else
    				{
    				$('.notice5').hide(500);
    				}
    		});
    		$('#text6').on('keyup',function(ev){
    			if($(this).val().length<1)
    				{
    				$('.notice6').show(500);
    				}
    			else
    				{
    				$('.notice6').hide(500);
    				}
    		});
    		$('#text7').on('keyup',function(ev){
    			if($(this).val().length<1)
    				{
    				$('.notice7').show(500);
    				}
    			else
    				{
    				$('.notice7').hide(500);
    				}
    		});
    	});
    </script>
</body>
</html>