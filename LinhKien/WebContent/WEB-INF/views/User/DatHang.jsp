<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="f" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Insert title here</title>
	<base href="${pageContext.servletContext.contextPath}/">
  <link rel="stylesheet"  href="Css/StyleUser.css">
  <link rel="stylesheet"  href="Css/StyleDatHang.css">
	<link rel="stylesheet"  href="Bootstrap/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"  href="Bootstrap/bootstrap/css/font-awesome.min.css">
    
    <link href="Bootstrap/bootstrap/css/mdb.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/7578a82dfb.js"></script>
</head>
<body>
	<header>
		<div class="container-fluid">
       
	       <div class="row">
	           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thanhheader">
               	 <div class="row">
                   
                   <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-push-1 image-logo" >
                       <img width="80" height="75" src="Bootstrap/bootstrap/logo.jpg" class="position-image"/>
                   </div>
                   
                   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-sm-push-1 search">
                       <input type="search" class="form-control-search"
                        name="" id="input" value="" required="required" placeholder="Bạn tìm gì...">
                          <button 
                               type="button"
                               class="button-search">
                               <span class="fa fa-search mr-5"/>
                         </button>
                   </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-sm-push-1" >
	                    <div class="row">

		                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin">
		                        	<i class="fas fa-cart-plus icon-user"></i>
		                        	<p class="admin">Giỏ hàng</p>
		                        	
		                        </a>
                             </div>
                             <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin" href="http://localhost:8080/LinhKien/user/kiemtra.htm">
		                        	<i class="fas fa-truck icon-user"></i>
		                        	<p class="admin">Kiểm tra</p>
		                        	
		                        </a>
                             </div>
                             <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin">
		                        	<i class="fas fa-home icon-user"></i>
		                        	<p class="admin">Trang chủ</p>
		                        	
		                        </a>
                             </div>
                             
	                   </div>
                    </div>
                   
                </div>
               </div>
	       </div>
       
       </div>
    </header>
    
    <!-- contexxt -->
    <br/>
    <br/>
    <br/>
    
     
    <p>${message}</p>
      
      <div class="container">
        
        <div class="row">
          
          <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <img width="90%" src="${ha}">
          </div>
          
          <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
            <div class="khunglon">
              <div class="texttensp">${tensp}</div>
              <div class="ngoisao">
                
                <i class="fa fa-star" style="color: rgb(255, 193, 32);"></i>
                <i class="fa fa-star" style="color: rgb(255, 193, 32);"></i>
                <i class="fa fa-star" style="color: rgb(255, 193, 32);"></i>
                <i class="fa fa-star" style="color: rgb(255, 193, 32);"></i>
                <i class="fa fa-star-o" style="color: rgb(255, 193, 32);"></i>
              </div>
              
              <div class="price"><p>Giá      ${gia}₫</p></div>
              <div class="vanchuyen"><p class="texttitle">Vận chuyển:</p> <i class="textnd">Miễn phí vận chuyển</i></div>
              <div class="form-dathang">
                  <div class="tang" onclick="tru()">-</div>
                  <div class="giam" onclick="them()">+</div>
                  <div class="chonvitri">Chọn số lượng</div>
                <form action="user/dathang.htm" method="POST" role="form">
                    <div class="test">
                    </div>
                    <input type="text" name="masp" value="${masp}" class="masp" id="" ><br>
                    <input type="text" name="makh" value="${makh}" class="makh" id="" ><br>
                  <button type="submit" class="btn btn-primary btn-dathang">Đặt hàng</button>
                </form>
                
              </div>
            </div>
          </div>
          
        </div>
        
      </div>
      
      
    <!-- footer -->
    <br/>
    <br/>
	<footer>
        <div class="footer-tgdd">
          <ul class="ul1">
            <li><a>Tìm hiểu về mua trả góp</a></li>
            <li><a>Chính sách bảo hành</a></li>
            <li><a>Chính sách đổi trả</a></li>
            <li><a>Giao hàng & Thanh toán</a></li>
            <li>
            <div class="dropdown">
              <div  data-toggle="dropdown">Xem thêm
              <span class="caret"></span></div>
              <ul class="dropdown-menu">
                <li><a href="#">Hướng dẫn mua online</a></li>
                <li><a href="#">Mua hàng doanh nghiệp</a></li>
                <li><a href="#">Phiếu mua hàng</a></li>
                <li><a href="#">In hóa đơn điện tử</a></li>
                <li><a href="#">Quy chế hoạt động</a></li>
                <li><a href="#">Nội quy cửa hàng</a></li>
                <li><a href="#">Chất lượng phục vụ</a></li>
                <li><a href="#">Thông tin trao thưởng</a></li>
                <li><a href="#">Chính sách khui hộp sản phẩm Apple</a></li>
              </ul>
            </div>
           </li>
          </ul>
          <ul class="ul2">
            <li><a>Giới thiệu công ty (mwg.vn)</a></li>
            <li><a>Tuyển dụng</a></li>
            <li><a>Gửi góp ý, khiếu nại</a></li>
            <li><a>Tìm siêu thị </a></li>
            <li><a>Xem bản mobile</a></li>
          </ul>
          <ul class="ul2">
            <li><a>Gọi mua hàng &nbsp; 1800.1060</a></li>
            <li><a>Gọi khiếu nại &nbsp; &nbsp;  1800.1062</a></li>
            <li><a>Gọi bảo hành &nbsp; &nbsp; 1800.1064</a></li>
            <li><a>Kĩ thuật &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 1800.1763</a></li>
            <li><a><i class="icontgdd-bct"></i></a></li>
          </ul>
          <ul class="ul2">
            <li>
              <a><i class="fab fa-facebook-square"></i>&nbsp; 3.3tr</a>
              &nbsp;
              &nbsp;
              <a><i class="fab fa-youtube"></i>  624.2k</a>
            </li>
            
            
          </ul>
        </div>
      </footer>
      <script src="Js/xuliUserLogin.js"></script>
	<script src="Bootstrap/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="Bootstrap/bootstrap/js/bootstrap.min.js"></script>
    <script>
        var giatri=1;
        var html="";
        html="<input"+" "+"class='soluong'"+" "+"name='soluong'" + " "+"value="+"'"+giatri+"'"+" "+"readonly"+">";
          $(".test").html(html);
        function them()
        {
         
         
          giatri=giatri+1;
          console.log(giatri);
          html="<input"+" "+"class='soluong'"+" "+"name='soluong'" + " "+"value="+"'"+giatri+"'"+" "+"readonly"+">";
          $(".test").html(html);
          console.log(html);
        }
        function tru()
        {
          if(giatri<=1)
          {
            giatri=1;
          }
         if(giatri>1)
         {
          giatri=giatri-1;
          console.log(giatri);
          html="<input"+" "+"class='soluong'"+" "+"name='soluong'" + " "+"value="+"'"+giatri+"'"+" "+"readonly"+">";
          $(".test").html(html);
         }
        }
       
      </script>
</body>
</html>