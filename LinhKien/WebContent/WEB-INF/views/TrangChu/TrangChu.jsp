<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="f" %> 

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Insert title here</title>
<base href="${pageContext.servletContext.contextPath}/">
	<link rel="stylesheet"  href="Bootstrap/bootstrap/css/Style.css">
	<link rel="stylesheet"  href="Css/Style1.css">
	<link rel="stylesheet"  href="Bootstrap/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"  href="Bootstrap/bootstrap/css/font-awesome.min.css">
    
    <link href="Bootstrap/bootstrap/css/mdb.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/7578a82dfb.js"></script>
</head>
<body>
	<header>
		<div class="container-fluid">
       
	       <div class="row">
	           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thanhheader">
               	 <div class="row">
                   
                   <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-push-1 image-logo" >
                       <a><img width="80" height="75" src="Bootstrap/bootstrap/logo.jpg" class="position-image"/></a>
                   </div>
                   
                   <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-sm-push-1 search">
                       <input type="search" class="form-control-search"
                        name="" id="input" value="" required="required" placeholder="Bạn tìm gì...">
                          <button 
                               type="button"
                               class="button-search">
                               <span class="fa fa-search mr-5"/>
                         </button>
                   </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-sm-push-1" >
	                    <div class="row">

		                     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin">
		                        	<i class="fas fa-cart-plus icon-user"></i>
		                        	<p class="admin">Giỏ hàng</p>
		                        	
		                        </a>
                             </div>
                             <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin">
		                        	<i class="fas fa-truck icon-user"></i>
		                        	<p class="admin">Kiểm tra</p>
		                        	
		                        </a>
                             </div>
                             <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
		                        <a class="link-admin">
		                        	<i class="fas fa-users icon-user"></i>
		                        	<p class="admin">${k}</p>
		                        	
		                        </a>
                             </div>
                             
	                   </div>
                    </div>
                   
                </div>
               </div>
	       </div>
       
       </div>
    </header>
    <br/>
    <br/>
      <div class="container">
        
        <div class="row">
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="carousel-id" class="carousel slide slide-image" data-ride="carousel">
              <ol class="carousel-indicators">
                  <li data-target="#carousel-id" data-slide-to="0" class=""></li>
                  <li data-target="#carousel-id" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-id" data-slide-to="2" class="active"></li>
                  <li data-target="#carousel-id" data-slide-to="3" class=""></li>
                  <li data-target="#carousel-id" data-slide-to="4" class=""></li>
              </ol>
              <div class="carousel-inner">
                  <div class="item">
                      <img data-src="holder.js/900x500/auto/#777:#7a7a7a/text:First slide" alt="First slide" src="https://cdn.shop.webike.vn/images/shopping/sliders/20170802_special_vn_1280_345.jpg"
                      width="1200" height="300"
                      >
                      <div class="container">
                          
                      </div>
                  </div>
                  <div class="item">
                      <img data-src="holder.js/900x500/auto/#666:#6a6a6a/text:Second slide" alt="Second slide" src="https://cdn.shop.webike.vn/images/shopping/banners/weekly-sale/20191002/20191002-quizbanner-1280x345.jpg"
                      width="1200" height="300"
                      >
                      <div class="container">
                          
                      </div>
                  </div>
                  <div class="item active">
                      <img data-src="holder.js/900x500/auto/#555:#5a5a5a/text:Third slide" alt="Third slide" src="https://cdn.shop.webike.vn/images/shopping/sliders/20171213_warehouse_clearance_vn_1280_345.jpg"
                      width="1200" height="300"
                      >
                      <div class="container">
                          
                      </div>
                  </div>
                  <div class="item">
                    <img data-src="holder.js/900x500/auto/#666:#6a6a6a/text:Second slide" 
                    width="1200" height="300"
                    alt="Second slide" src="https://cdn.shop.webike.vn/images/shopping/sliders/20180726_suzuka_webike_support_vn_1280_345.jpg">
                    <div class="container">
                        
                    </div>
                  </div>
                  <div class="item">
                    <img width="1200" height="300"
                     data-src="holder.js/900x500/auto/#666:#6a6a6a/text:Second slide" alt="Second slide" src="https://cdn.shop.webike.vn/images/shopping/sliders/20170614_shipping_banner_vn_1280_345.jpg">
                    <div class="container">
                        
                    </div>
                  </div>
              </div>
              <!-- Left and right controls -->
              <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
              <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
          </div>
          </div>
          
        </div>
        
      </div>
      
    <!-- contexxt -->
    <br/>
    <br/>
      <div class="container">
        
        <div class="row">
          
          <c:forEach var="l" items="${list}">
          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <div class="hienthisanpham">
                <div class="thongtinsanpham">
                  <img width="260" height="200"  src="${l.getHinhanh()}">
                  <p>
                     ${l.getTensp()}           </p>
                  <p>${l.getGia()}₫</p>
                </div>
                <div class="form">
                    
                      <div class="form-aniput">
                        <input name="masp" value="${l.getMasp()}">
                      </div>
                      <a href="http://localhost:8080/LinhKien/user/dathang.htm?masp=${l.getMasp()}&&makh=${k}&&tensp=${l.getTensp()}&&gia=${l.getGia()}&&ha=${l.getHinhanh()}"><button class="btn btn-primary"><i class="fas fa-cart-plus"></i></button></a>
                    
                  </div>  
            </div>
          </div>
          </c:forEach>
        </div>
      </div>
      
      

    <!-- footer -->
    <br/>
    <br/>
	<footer>
        <div class="footer-tgdd">
          <ul class="ul1">
            <li><a>Tìm hiểu về mua trả góp</a></li>
            <li><a>Chính sách bảo hành</a></li>
            <li><a>Chính sách đổi trả</a></li>
            <li><a>Giao hàng & Thanh toán</a></li>
            <li>
            <div class="dropdown">
              <div  data-toggle="dropdown">Xem thêm
              <span class="caret"></span></div>
              <ul class="dropdown-menu">
                <li><a href="#">Hướng dẫn mua online</a></li>
                <li><a href="#">Mua hàng doanh nghiệp</a></li>
                <li><a href="#">Phiếu mua hàng</a></li>
                <li><a href="#">In hóa đơn điện tử</a></li>
                <li><a href="#">Quy chế hoạt động</a></li>
                <li><a href="#">Nội quy cửa hàng</a></li>
                <li><a href="#">Chất lượng phục vụ</a></li>
                <li><a href="#">Thông tin trao thưởng</a></li>
                <li><a href="#">Chính sách khui hộp sản phẩm Apple</a></li>
              </ul>
            </div>
           </li>
          </ul>
          <ul class="ul2">
            <li><a>Giới thiệu công ty (mwg.vn)</a></li>
            <li><a>Tuyển dụng</a></li>
            <li><a>Gửi góp ý, khiếu nại</a></li>
            <li><a>Tìm siêu thị </a></li>
            <li><a>Xem bản mobile</a></li>
          </ul>
          <ul class="ul2">
            <li><a>Gọi mua hàng &nbsp; 1800.1060</a></li>
            <li><a>Gọi khiếu nại &nbsp; &nbsp;  1800.1062</a></li>
            <li><a>Gọi bảo hành &nbsp; &nbsp; 1800.1064</a></li>
            <li><a>Kĩ thuật &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 1800.1763</a></li>
            <li><a><i class="icontgdd-bct"></i></a></li>
          </ul>
          <ul class="ul2">
            <li>
              <a><i class="fab fa-facebook-square"></i>&nbsp; 3.3tr</a>
              &nbsp;
              &nbsp;
              <a><i class="fab fa-youtube"></i>  624.2k</a>
            </li>
            
            
          </ul>
        </div>
      </footer>
	
	<script src="Bootstrap/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="Bootstrap/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>